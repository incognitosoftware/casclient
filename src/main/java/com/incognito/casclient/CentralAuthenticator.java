package com.incognito.casclient;

import com.incognito.eco.cas.restclient.CasRestClient;
import com.incognito.eco.cas.restvo.LoginRequest;
import com.incognito.eco.cas.restvo.User;
import com.incognito.eco.cas.restvo.UserLoginResponse;
import com.incognito.restclient.RestManager;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

public class CentralAuthenticator {
  private static CasRestClient casRestClient = null;
  private static String adminUsername = "admin";
  private static String adminPassword = "incognito";
  private static String token;
  private static String casHost;
  private static int casPort;

  private static final CentralAuthenticator instance = new CentralAuthenticator();

  enum UserType
  {
    USER,
    ADMIN
  }

  /**
   * Get an instance of CAS login. First instance is logged in with adminUsername and adminPassword.
   */
  public static CentralAuthenticator getInstance(String host, int port) {
    casHost = host;
    casPort = port;
    if (casRestClient == null) {
      login(adminUsername, adminPassword);
    }

    return instance;
  }
  
  /**
   * Log in with the client as a user via CAS.
   *
   * @param username
   * @param password
   * @return
   */
  public static UserLoginResponse login(String username, String password) {
    casRestClient = connect(casHost, casPort);
    LoginRequest loginVo = new LoginRequest();
    loginVo.setUsername(username);
    loginVo.setPassword(password);
    UserLoginResponse loginResp = casRestClient.login(loginVo);
    casRestClient.getManager().setAuthToken(loginResp.getAuthorization());

    return loginResp;
  }

  /**
   * Logout the client via CAS.
   *
   */
  public static void logout() {
    casRestClient.logout();
  }

  /**
   * Create an admin user.
   * @param username
   * @param password
   * @return
   */
  public User createAdminUser(String username, String password)
  {
    return createUser(username, password, UserType.ADMIN);
  }

  /**
   * Create a local user.
   * @param username
   * @param password
   * @return
   */
  public User createLocalUser(String username, String password)
  {
    return createUser(username, password, UserType.USER);
  }

  public User createUser(String username, String password, UserType type)
  {
    User user = new User();
    user.setUsername(username);
    user.setPassword(password);
    user.setType(type.name());

    return casRestClient.createUser(user);
  }
  
  public CasRestClient getCasRestClient()
  {
    return casRestClient;
  }

  public static CasRestClient connect(String casHost, int port) {
    if (casHost.isEmpty()) {
      throw new RuntimeException("casHost is not configured");
    }

    List<URI> uris = new ArrayList<>();
    try {
      uris.add(new URI("https://" + casHost + ":" + port));
    } catch (URISyntaxException e) {
      throw new RuntimeException("Failed to resolve the URI for cas");
    }

    RestManager.setReadTimeout(1000000); // make time out long for debugging
    RestManager manager = new RestManager((String) null, true, uris);

    return new CasRestClient(manager);
  }

  /**
   * Get authorization token from the instance.
   *
   */
  public String getAuthToken() {

    token = casRestClient.getManager().getAuthToken();
    return token;
  }
}
